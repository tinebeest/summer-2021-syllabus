# Summer 21:

## Goals overall

* feel confident sharing a draft with co-authors for the Confucianism project
* be at the point where you can submit the article to a first rate journal for the Goguryeo chapter
* have a full draft of the manuscript (rough) and a reworked chapter 1 that is more in line with most recent scholarship, having worked through Naomi's comments
* have an ungraded syllabus ready to roll out for all courses (FYS, HST269, HST380)

Extra curricular activities:

* Be able to run 10 miles (if headaches allow)
* play Suite Modale in full, and move onto page 2 in the Hindemith Sonata
* Made a start on the classic works of the field of sinology and history
    - create a specific list!

### General reading list:

* [ ] *Japanese for Sinologists*
* [ ]  Thomas Mullaney, ed., *Critical Han Theory*
* [ ]  Nick Tackett, *The Origins of the Chinese Nation*
* [ ]  Shao-Yun Yang, *The Way of the Barbarians*

### Useful to read
* [ ]  Chicago Manual of Style
* [ ]  Wilkinson, *Chinese History: A New Manual*, 5th ed.


### Catch-up on the literature reading list: (aspirational)
* [ ]  Chen, *Multicultural China in the Early Middle Ages*
* [ ]  Geertz, *the_intepretation_of_cultures*
* [ ]  ?maybe? Kantorowicz, *The King's Two Bodies_ A Study in Mediaeval Political Theology*
* [ ]  Weber, [need to figure out which one everybody always refers to in my field]
* [ ]  ? 

## Method:
Reading lists and writing tasks for every week.
Complete these to earn a relaxed weekend (but continue the streak on the manuscript as a matter of honour).

===========
### Week 1:

**Orientation week**

* drafting syllabus
* Various workshops

#### Readings
- Grosjean, "Complementarity Principle"
	+ (linguistics) literacies and bi-/multi-lingualism in the Balhae context
- Tan Qixiang, *Dongbei lishi dili*
	+ (historical geography) borders of Balhae as defined in his work
	+ cross reference with others
- Ungrading: chapters for book club: Saturday

==========

### Week 2:

**Summer Holiday**

May 31 - June 6

A much needed break: only keep up the manuscript streak, runs, and some flute playing

==========

### Week 3: Info dump and write it all up
**Summer Advising Part 1**

- **Readings**:
    + [ ] LVAIC Community Read: read chapters 1-2
- **Writing**:
    + For all three projects: get as much as possible out, massive freewrite style, we'll clean it up later. This has worked in the past.
        + [ ] **Book MS**: Check comments intro/chapter 1 and write a first response
            * reading up on ethnicity as that is a thorny question that I do need to address in the Notes or the intro:
                *  do I address it? why not if I don't?
                *  what terms do I use to refer to "Chinese" (i.e. ethnic Chinese/Han of the Tang state)
                *  Suggestion from Shao-Yun Yang: *huaxia* can be useful, for ethnicity [I already use it for territory]; for foreigners: *fan* is more neutral, *yi* to indicate "barbarian, uncultured, uncivilized"
        + [ ] **Goguryeo**: ethnicity/diaspora thoughts and perhaps how to adjust the paper as it currently stands
            * includes similar issue to **Book MS**: ethnicity
        + [ ] **Confucanism**: draft as much as possible of what you think this may look like. Center around Balhae as an example?
            * but other polities, and timeline also can be featured in a useful way

* June 7 (Monday)
    - AM writing
    - Summer advising workshop (2-4PM)
    - Coaching session (4-4.50PM)
* June 8 (Tuesday)
    - Summer advising availability 12-4PM
* June 9 (Wednesday)
    - Good day for writing
* June 10 (Thursday)
    - AM still plenty of writing time
    - 9-10AM: LVAIC community read: *The Inclusive Instructor*
    - Summer advising availability 12-4PM
* June 11 (Friday)
    - early start with PT --> reschedule run day
    - LMU conference AM
	- Write on Site; 

#### Evaluation
**How did the week go?** Overall it felt really good, and I felt much more in control than I usually do during the summer weeks, especially early on. I was able to get much closer to the core of the issue with ethnicity and why it is so problematic to define (in part because it just *is*, in part because we use different solutions because we think we have different audiences in mind)

**What should I repeat/do more of?**

- Having concrete items listed in the weekly outline here helps me when I feel a bit stuck: create the list of "ingredients" or "bites"!
- Eat the frogs first! Just do it, it's ok, and if not, you have time to sort it out!
- Remember: It's better to ask questions and look stupid but learn, than to look smart and not ask questions, but remain stupid.

**What should I try to avoid?/What was not ideal?** Struggling a bit to get going in the mornings (not anywhere near as much as usual), so definitely making sure I am *much* better about bed time. Hot summer nights don't help, though...

(No, we don't give grades in this course!)

==========
### Week 4: Japan!
**Summer Advising Part 2**

* **Readings**
    - [ ] Piggott: photocopied chapters #CamConf
    - [ ] dip into the primary sources: *Ruiju kokushi* #CamConf
        + [ ] check out Jiang, Chengshan 姜成山 article on Balhae in Ruiju kokushi #CamConf #MS
    + [ ]  Bai Genxing article #Gog
    + [ ]  Look into Goguryeoans fleeing to Japan? --> remember: not main point, but may be useful for #MS, possibly no more than brief mention in #Gog.

* **Writing tasks**
    - [ ] make sure you write every day on the project you are working on: reading ≠ writing!
        + [ ] summarize main ideas and tie back to what you need for progress on particular project (don't get distracted by the shiny!)

### Schedule: 
* June 14 (Monday)
    - 12-3PM Summer advising
    - 4-4.50 coaching session
* June 15 (Tuesday)
    - 10-12am: Write on Site Annex
* June 16 (Wednesday)
    - 12-4PM Summer advising availability
* June 17 (Thursday)
    - 9-10 AM LVAIC community read
    - 10.15-11: flute lesson
    - 12-4PM: Summer advising availability
* June 18 (Friday)
    - 12-1: Write on site
    - 1-4.20PM: TAA conference Day 1
* June 19 (Saturday)
    - LMU conference Day 4 9-11.30 (skipped day 3)
    - #Ungrading Zoom 12-1 (optional)

==========

### Week 5: Law and legal structures
**TAA conference PM**, but AM good for writing

* **Readings**:
    - [ ] Ooms: Ancient Japanese Law #CamConf
    - [ ] Tang Code: sections on foreigners #CamConf #Gog
    - [ ] ? maybe *Liji* or *Kaiyuan li* on *Honglusi* for reception of foreign envoys?
    - [ ] Bai Genxing materials #Gog
* **Writing**:
    - [ ] Write out basics of legal system and look for similarities/big picture
        + little known about Balhae but can perhaps extrapolate? Lots about Japan's adaptations, can be useful/interesting for #CamConf
    - [ ] legal status of foreigners in Tang: useful for #Gog
        + find out where to fit in

* June 21 (Monday)
    - 2-4.30PM TAA conference
* June 22 (Tuesday)
    - 2-5PM TAA conference
* June 23 (Wednesday)
    - 2-4.30PM TAA conference
    - 4-5.30PM Africana book club (attend final part)
* June 24 (Thursday)
    - 9-10: LVAIC community read
    - 2-4.30PM TAA conference
* June 25 (Friday)

==========

### Week 6:
#### Buddhism and the state
* **Readings**: Jülch: *The Middle Kingdom and the Dharma Wheel*, 1-217
    + [ ]Identify the chapters that are most useful to understand Buddhism in relation to the state #CamConf #MS
        * Issues of legitimacy (see also Vermeersch)
    + Useful for quick references for common knowledge if needed for notes
- **Writing**:
    -  [ ] Buddhism as interwoven with state and authority #CamConf #MS (Goguryeo, Balhae, Intro)
    - [ ] write-up of ethnicity/diaspora section in full #Gog

* June 28 (Monday)
    - AM still good for writing!
    - 12-3PM: planning retreat
    - 3PM: coaching session
* June 29 (Tuesday)
* June 30 (Wednesday)
* July 1 (Thursday)
    - LVAIC community read
* July 2 (Friday)

==========
### Week 7: Books

- **Readings**:
    + [ ] Materials from Yao Ping on epitaphs #Gog
- **Writing**:
    + [ ] integrate materials from Yao Ping on epitaphs/princesses #Gog

* July 5 (Monday)
* July 6 (Tuesday)
* July 7 (Wednesday)
* July 8 (Thursday)
* July 9 (Friday)

==========
### Week 8: Legitimation of rulers and rulership
* July 12 (Monday)
* July 13 (Tuesday)
* July 14 (Wednesday)
* July 15 (Thursday)
* July 16 (Friday)

==========
### Week 9: Art forms and shared culture
* July 19 (Monday)
* July 20 (Tuesday)
* July 21 (Wednesday)
* July 22 (Thursday)
* July 23 (Friday)

==========
### Week 10: Crunchy time* - writing!

\* insider's joke :-) 

* July 26 (Monday)
* July 27 (Tuesday)
* July 28 (Wednesday)
* July 29 (Thursday)
* July 30 (Friday)

==========
### Week 11: Crunchy time - writing!
* August 2 (Monday)
* August 3 (Tuesday)
* August 4 (Wednesday)
* August 5 (Thursday)
* August 6 (Friday)

==========
### Week 12: Crunchy time - writing!
* August 9 (Monday)
* August 10 (Tuesday)
* August 11 (Wednesday)
* August 12 (Thursday)
* August 13 (Friday)

==========
### Week 13: Crunchy time - writing!
* August 16 (Monday)
* August 17 (Tuesday)
* August 18 (Wednesday)
* August 19 (Thursday)
* August 20 (Friday)

==========
### Week 14: Clean up/last buffer zone
**Clean-up week**: There will be meetings and other interruptions during this week, so don't count on the regular amount of working hours being available.

* August 23 (Monday)
* August 24 (Tuesday)
* August 25 (Wednesday)
* August 26 (Thursday)
* August 27 (Friday)
